﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace GenericVarianceDemo
{
    class Program
    {
        static void Main()
        {
            List<Ellenőr> ellenőrök = new List<Ellenőr>()
            {
                new Ellenőr { Név = "Géza" },
                new Ellenőr { Név = "Béla" },
            };
            IEnumerable<Ember> q1 = from e in ellenőrök
                where e.Név.StartsWith("G")
                select e;
            q1.First().Köszön();

            Action<Ellenőr> a = new Action<Ember>(
                ember => Console.WriteLine(ember.Név));

            List<Ember> emberek = new List<Ember>();
            emberek.Sort(new EmberComparer());
            ellenőrök.Sort(new EmberComparer());

            Console.ReadLine();
        }
    }

    internal class EmberComparer : IComparer<Ember>
    {
        public int Compare(Ember x, Ember y)
        {
            return x.Név.CompareTo(y.Név);
        }
    }

    class Ember
    {
        public string Név { get; set; }

        public void Köszön()
        {
            Console.WriteLine("Szia, én {0} vagyok.", Név);
        }
    }

    class Ellenőr : Ember
    {
        public new void Köszön()
        {
            Console.WriteLine("JEGYEKET, BÉRLETEKET!!! *habzik a szája*");
        }
    }

    interface IInterface<out T>
    {
        T M();
    }
}