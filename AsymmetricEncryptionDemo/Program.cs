﻿using System;
using System.Text;
using System.Security.Cryptography;

namespace AsymmetricEncryptionDemo
{
    class Program
    {
        static void Main()
        {
            //partner
            RSACryptoServiceProvider partnerRSA = new RSACryptoServiceProvider();
            RSAParameters publickey = partnerRSA.ExportParameters(false);
            Console.WriteLine("A partner nyilvános kulcsa: " + 
                partnerRSA.ToXmlString(false));
            Console.ReadLine();

            //én
            string message = "szalonna!";
            RSACryptoServiceProvider myRSA = new RSACryptoServiceProvider();
            myRSA.ImportParameters(publickey);
            byte[] messageBytes = Encoding.Unicode.GetBytes(message);
            byte[] encryptedBytes = myRSA.Encrypt(messageBytes, false);
            Console.WriteLine("Titkos üzenet: " + Encoding.Unicode.GetString(encryptedBytes));
            Console.ReadLine();

            //partner
            byte[] decryptedBytes = partnerRSA.Decrypt(encryptedBytes, false);
            Console.WriteLine("Visszafejtett: " + Encoding.Unicode.GetString(decryptedBytes));

            Console.ReadLine();
        }
    }
}