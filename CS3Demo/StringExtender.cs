﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS3Demo
{
    static class StringExtender
    {
        public static void Enumerate(this string[] items, Action<string> a)
        {
            foreach (var s in items)
            {
                a(s);
            }
        }
    }

    interface IBurnable
    {
        void BurnThis();
    }

    static class BurnableExtender
    {
        public static void Burn(this IBurnable item)
        {
            item.BurnThis();
            Console.WriteLine("Felgyújtottak!");
        }
    }

    static class DateTimeExtender
    {
        public static bool IsAfterFirstContact(this DateTime date)
        {
            return (date - new DateTime(2063, 04, 05)).TotalMilliseconds > 0;
        }
    }
}
