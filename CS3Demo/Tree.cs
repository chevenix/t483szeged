﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS3Demo
{
    class Tree : IBurnable
    {
        private int _bark = 7;
        public void BurnThis()
        {
            Console.WriteLine("Ennyi kérgem van még: {0}", _bark--);
        }
    }
}
