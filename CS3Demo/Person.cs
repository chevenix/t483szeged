﻿using System;

namespace CS3Demo
{
    class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string City { get; set; }

        public Person()
        {
            
        }

        public Person(string name)
        {
            Name = name;
        }
    }
}