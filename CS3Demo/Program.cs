﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CS3Demo
{
    class Program
    {
        static void Main()
        {
            //PartialMethods();
            //ArrayTypeInference();
            //CollectionInitializers();
            //ObjectInitializers();
            //AnonymousTypesAndVar();
            //LambdaExpressions();

            string[] ss = { "Alma", "Béka", "Cékla" };
            //StringExtender.Enumerate(ss, s => Console.WriteLine(s));
            ss.Enumerate(s => Console.WriteLine(s));

            Tree t = new Tree();
            t.Burn();

            DateTime dt1 = DateTime.Now;
            Console.WriteLine(dt1.IsAfterFirstContact());

            DateTime dt2 = new DateTime(2070,1,1);
            Console.WriteLine(dt2.IsAfterFirstContact());


            Console.ReadLine();
        }



        private static void LambdaExpressions()
        {
//Func<int, int, string> f1 = delegate(int a, int b)
            //    {
            //        return (a + b).ToString();
            //    };
            Func<int, int, string> f1 = (a, b) => (a + b).ToString();
            Console.WriteLine(f1(1, 2));

            Action<int> a1 = a => Console.WriteLine(a);
        }

        private static void AnonymousTypesAndVar()
        {
            var ppl = new List<Person>
            {
                new Person("Stan") {Age = 8, City = "South Park"},
                new Person {Name = "Kyle", Age = 8, City = "South Park"},
                new Person {Name = "Eric", Age = 7, City = "South Park"},
            };

            var o = new {Name = "Stan", Age = 8, Kutya = 66};
            //object o2 = new {Age = 8, Name = "Stan",Kutya = 66};
            Console.WriteLine(o.ToString());
            Console.WriteLine(o.GetType().FullName);
            Console.WriteLine(o.Name);
        }

        private static void ObjectInitializers()
        {
            List<Person> ppl = new List<Person>
            {
                new Person("Stan") {Age = 8, City = "South Park"},
                new Person {Name = "Kyle", Age = 8, City = "South Park"},
                new Person {Name = "Eric", Age = 7, City = "South Park"},
            };
        }

        private static void CollectionInitializers()
        {
            List<Person> ppl = new List<Person>
            {
                new Person(),
                new Person(),
                new Person()
            };

            Dictionary<string, Person> dict = new Dictionary<string, Person>
            {
                {"alma", new Person()},
                {"béka", new Person()},
                {"cékla", new Person()},
            };
        }

        private static void ArrayTypeInference()
        {
            string[] ss = {"alma", "béka"};
        }

        private static void PartialMethods()
        {
            var tc = new TestClass();
            tc.TestAll();
        }
    }
}