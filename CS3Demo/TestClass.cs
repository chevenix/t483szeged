﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS3Demo
{
    partial class TestClass
    {
        public int I { get; private set; }

        public void TestAll()
        {
            TestA();
            TestB();
        }

        partial void TestA();
        partial void TestB();

    }
}