﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;

namespace JsonSerilaizationDemo
{
    class Program
    {
        static void Main()
        {
            DataContractJsonSerializer bf = new DataContractJsonSerializer(typeof(Person));
            if (File.Exists("stan.json"))
            {
                using (var fs = File.Open("stan.json", FileMode.Open))
                {
                    Person p = bf.ReadObject(fs) as Person;
                    Console.WriteLine("Név: {0}, kor: {1}, született: {2}",
                        p.Name, p.Age, p.BirthDate);
                }
            }
            else
            {
                Person p = new Person
                {
                    Name = "Stan",
                    Age = 8,
                    BirthDate = new DateTime(2009, 4, 6)
                };

                using (var fs = File.Create("stan.json"))
                {
                    bf.WriteObject(fs, p);
                    Console.WriteLine("sorosítva");
                }
            }
            Console.ReadLine();
        }
    }
}
