﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace JsonSerilaizationDemo
{
    [DataContract]
    public class Person
    {
        [DataMember]
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        [DataMember]
        private int _age;
        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        [DataMember]
        private DateTime _birthDate;
        public DateTime BirthDate
        {
            get { return _birthDate; }
            set { _birthDate = value; }
        }

    }
}
