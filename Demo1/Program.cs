﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using Animals.Mammals;
using Szomszédok;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Xml;
using Dog = Animals.Mammals.Dog;

namespace Demo1
{
    class Program
    {
        static int Main(string[] args)
        {
            //Valtozok();
            /*Tombok();
             pifnvcn
             fvsdvfd
             */
            //Gyujtemenyek();
            //SwitchTest();
            //For();
            //Foreach();
            GetInt();

            Console.ReadLine();
            return 0;
        }

        private static void GetInt()
        {
            Console.Write("Adj meg egy integert: ");
            string s = Console.ReadLine();
            int j;
            bool b = int.TryParse(s, out j);
            if (b)
            {
                //int i = int.Parse(s);
                Console.WriteLine(j);
            }
        }
        

        private static void Foreach()
        {
            object[] ok = new object[] {1, 2, 3, 20.4};
            int sum = 0, dif = 0;
            for (int i = 0; i < ok.Length; i++)
            {
                sum += (int)ok[i];
                dif -= (int)ok[i];
            }

            foreach (int item in ok)
            {
                sum += item;
                dif -= item;
            }
        }

        private static void For()
        {
            int i = 77;
            for (;;)
            {
                if (i % 2 == 0)
                {
                    i++;
                    continue;
                }
                if (i++ >= 100)
                {
                    break;
                }
                Console.WriteLine(i);
            }
        }

        private static void SwitchTest()
        {
            Console.Write("Add meg a neved: ");
            string s = Console.ReadLine();
            switch (s.ToUpper())
            {
                case "ALMA":
                case "KÖRTE":
                    Console.WriteLine("Gyümölcs");
                    break;
                case "BÉKA":
                    Console.WriteLine("Állat");
                    break;
                case "PADLIZSÁN": throw new Exception();
                case "GARFIELD":
                    return;
                default:
                    Console.WriteLine("Nem állat és nem gyümölcs.");
                    break;
            }
        }

        private static void Gyujtemenyek()
        {
            List<int> intek = new List<int>();
            intek.Add(50);
            intek[0] = 100;
            intek.Insert(0, 99);
            intek.Remove(100);
            int index = intek.IndexOf(100);


            Dictionary<string, int> dict = new Dictionary<string, int>();
            dict.Add("alma", 1);
            dict.Add("béka", 2);
            dict.Add("cékla", 43);
            //dict.Add("alma", 10);
            dict["alma"] = 77;

            Stack<int> stack = new Stack<int>();
            for (int i = 0; i < 10; i++)
            {
                stack.Push(i);
            }
            while (stack.Count > 0)
            {
                Console.WriteLine(stack.Pop());
            }

            BitVector32 bv = new BitVector32(34323);
            Console.WriteLine(bv);

            BitArray ba1 = new BitArray(10);
            BitArray ba2 = new BitArray(10);
            BitArray ba3 = ba1.Xor(ba2);
        }

        private static void Valtozok()
        {
            Object o1;
            o1 = new object();
            object o2 = new object();
            o1 = o2;
            object o3 = new object();
            Int32 i = 8;
            Single s = new float();

            int j = 7, k = 8;
            object o4 = null;
            j = 99;
            Console.WriteLine("J: {0:C}, k: {1}", j, k);
            Animals.Mammals.Dog d = new Dog();
        }

        private static void Tombok()
        {
            int[] ik = new int[] {1, 2, 3};
            Array.Resize(ref ik, 10);
            //Console.WriteLine(ik[10]);
            object[] ok = new string[1];
            ok[0] = new object();
        }
    }
}