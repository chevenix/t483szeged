﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructDemo
{
    class Program
    {
        static void Main()
        {
            StructTest();
            BoxingTest();

            Console.ReadLine();
        }

        private static void BoxingTest()
        {
            object o;
            int i = 42;
            o = i;
            Console.WriteLine(o.ToString());
            //o++;

            int j = (int)o;
            j++;

            object[] ok = new object[] { 1, 2, 3};
        }

        private static void StructTest()
        {
            Point p3 = new Point();

            Point p1 = new Point(8);
            p1.Y = 1;
            Point p2 = new Point(5);
            p2.Y = 3;

            p1.X = 11;
            p2 = p1;
            p2.X = 15;

            Console.WriteLine(p1.X);
        }
    }

    struct Point
    {
        private int _x;
        public int X
        {
            get { return _x; }
            set { _x = value; }
        }

        private int _y;
        public int Y
        {
            get { return _y; }
            set { _y = value; }
        }

        //public Point()
        //{
        //}

        public Point(int x)
        {
            _x = x;
            _y = 0;
        }
    }
}