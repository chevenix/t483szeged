﻿using System;
using System.Text;

namespace IDisposableDemo
{
    class MyClass : IDisposable
    {
        StringBuilder _sb = new StringBuilder();
        private bool _isDisposed = false;

        public void Append(string s)
        {
            if (_isDisposed)
                throw new ObjectDisposedException("x", "Én már nem is vagyok");
            _sb.Append(s);
            Console.WriteLine("Jelenlegi tartalmam: " + _sb);
        }


        protected virtual void Dispose(bool disposing)
        {
            if (_isDisposed == false)
            {
                _isDisposed = true;
                _sb.Clear();

                if (disposing)
                {
                    _sb = null;
                }
                GC.SuppressFinalize(this);
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        ~MyClass()
        {
            Dispose(false);
        }
    }

    class MyDerivedClass : MyClass
    {
        StringBuilder _derivedSb = new StringBuilder();

        protected override void Dispose(bool disposing)
        {
            _derivedSb.Clear();
            base.Dispose(disposing);
        }
    }
}