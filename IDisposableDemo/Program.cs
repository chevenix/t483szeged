﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDisposableDemo
{
    class Program
    {
        static void Main()
        {
            MyClass mc = new MyClass();
            //mc = null;
            mc.Append("alma");
            mc.Dispose();
            mc.Append("béka");

            Console.ReadLine();
        }
    }
}