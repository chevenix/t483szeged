﻿using System;
using System.Runtime.Serialization;

namespace BinarySerializationDemo
{
    [Serializable]
    class Person : IDeserializationCallback
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        [NonSerialized]
        private int _age;
        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        private DateTime _birthDate;
        public DateTime BirthDate
        {
            get { return _birthDate; }
            set { _birthDate = value; }
        }

        public void OnDeserialization(object sender)
        {
            _age = (int)((DateTime.Now - _birthDate).TotalDays/365.25);
        }
    }
}