﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace BinarySerializationDemo
{
    class Program
    {
        static void Main()
        {
            BinaryFormatter bf = new BinaryFormatter();
            if (File.Exists("stan.txt"))
            {
                using (var fs = File.Open("stan.txt", FileMode.Open))
                {
                    Person p = bf.Deserialize(fs) as Person;
                    Console.WriteLine("Név: {0}, kor: {1}, született: {2}", 
                        p.Name, p.Age, p.BirthDate);
                }
            }
            else
            {
                Person p = new Person { Name = "Stan",
                    Age = 8, BirthDate = new DateTime(2009, 4, 6)};

                using (var fs = File.Create("stan.txt"))
                {
                    bf.Serialize(fs, p);
                    Console.WriteLine("sorosítva");
                }
            }
            Console.ReadLine();
        }
    }
}