﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TPLDemo
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("MAIN: " + Thread.CurrentThread.ManagedThreadId);
            Task t1 = new Task(Doer.DoSomething, ConsoleColor.Cyan);
            Task t2 = new Task(Doer.DoSomething, ConsoleColor.Yellow);

            //t1.Start();
            //t2.Start();

            //t1.Wait();
            //t2.Wait();
            //Task.WaitAll(t1, t2);
            //int index = Task.WaitAny(t1, t2);


            //Parallel.Invoke(() => { }, () => { });

            Task<int> t3 = new Task<int>(o =>
            {
                if (o is string)
                {
                    return Doer.GetLength((string) o);
                }
                return -1;
            }, "alma");

            t3.Start();
            int i = t3.Result;
            Console.WriteLine(i);

            Console.WriteLine("Mindnek vége!");
            Console.ReadLine();
        }
    }

    public class Doer
    {
        private static readonly object _locker = new object();
        public static void DoSomething(object o)
        {
            ConsoleColor cc = (ConsoleColor)o;
            for (int i = 0; i < 10; i++)
            {
                lock (_locker)
                {
                    Console.ForegroundColor = cc;
                    Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
                }
                //Thread.Sleep(100);
                Task.Delay(100).Wait();
            }
        }

        public static int GetLength(string s)
        {
            Task.Delay(3000).Wait();
            return s.Length;
        }
    }
}