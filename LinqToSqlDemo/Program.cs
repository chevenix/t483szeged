﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace LinqToSqlDemo
{
    class Program
    {
        static void Main()
        {
            NorthwindDataContext ctx = new NorthwindDataContext();
            ctx.Log = Console.Out;

            //var alfki = (from c in ctx.Customers
            //    where c.CustomerID == "ALFKI"
            //    select c).Single();
            //foreach (var order in alfki.Orders)
            //{
            //    Console.WriteLine(order.Order_Details.First().Product.ProductName);
            //}

            //var evosoft = new Customer() { CompanyName = "evosoft", CustomerID = "EVSFT"};
            //ctx.Customers.InsertOnSubmit(evosoft);
            //ctx.SubmitChanges();

            foreach (var item in ctx.Ten_Most_Expensive_Products())
            {
                Console.WriteLine(item.TenMostExpensiveProducts + " " + item.UnitPrice);
            }

            Console.ReadLine();
        }
    }
}