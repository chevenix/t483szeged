﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS4Demo
{
    class Program
    {
        static void Main()
        {
            var mx = GenerateMatrix(5, defaultValue: 9);

            Console.ReadLine();
        }

        static int[,] GenerateMatrix(int x, int y = 6, int defaultValue = 7)
        {
            int[,] result = new int[x,y];
            for (int j = 0; j < result.GetLength(1); j++)
                for (int i = 0; i < result.GetLength(0); i++)
                    result[i, j] = defaultValue;
            return result;
        }

        //private static int[,] GenerateMatrix(int x, int y)
        //{
        //    return GenerateMatrix(x, y, 7);
        //}
    }
}