﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CounterDemo
{
    class Program
    {
        static void Main()
        {
            for (int i = 0; i < 10000000; i++)
            {
                new Counter();
                Console.WriteLine(Counter.Count);
            }
        }
    }

    class Counter
    {
        private static int _count;
        public static int Count
        {
            get { return _count; }
        }

        public Counter()
        {
            _count++;
        }

        ~Counter()
        {
            _count--;
        }
    }
}