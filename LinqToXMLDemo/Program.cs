﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LinqToXMLDemo
{
    class Program
    {
        static void Main()
        {
            //CreateXML();

            LinqToXML();

            Console.ReadLine();
        }

        private static void LinqToXML()
        {
            XDocument xd = XDocument.Load("products.xml");
            //Console.WriteLine(xd);

            var items = from e in xd.Descendants("product")
                where (int) e.Element("price") > 70
                orderby (string) e.Element("name") descending 
                select new {Name = (string)e.Element("name"),
                    Id = (int)e.Attribute("id")};
            foreach (var item in items)
            {
                Console.WriteLine(item);
            }
        }

        private static void CreateXML()
        {
            XElement root = new XElement("products",
                new XElement("product",
                    new XAttribute("id", 0),
                    new XElement("name", "bor"),
                    new XElement("price", 100)),
                new XElement("product",
                    new XAttribute("id", 1),
                    new XElement("name", "sör"),
                    new XElement("price", 50)),
                new XElement("product",
                    new XAttribute("id", 2),
                    new XElement("name", "pálinka"),
                    new XElement("price", 250))
                );
            Console.WriteLine(root);

            XDocument outdoc = new XDocument(root);
            outdoc.Save("products.xml");
        }
    }
}