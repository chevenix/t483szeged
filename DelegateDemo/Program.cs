﻿using System;
using System.Linq;
using System.Runtime.InteropServices;

namespace DelegateDemo
{
    delegate void MyDelegate(string s);

    class Program
    {
        static void Main(string[] args)
        {
            string[] ss = new string[] { "Alma", "Béka", "Cékla" };
            StringMethods sm = new StringMethods();

            MyDelegate md1 = sm.PrintFirst;
            md1 += new MyDelegate(sm.PrintLast);
            md1 += new MyDelegate(sm.PrintFirst);
            md1 += sm.PrintObject;
            md1 += delegate(string s)
            {
                Console.WriteLine(new string(s.Reverse().ToArray()));
            };

            DoSomething(ss, md1);

            //MyDelegate md2 = new MyDelegate(sm.PrintFirst);
            //md2+= new MyDelegate(sm.PrintFirst);
            //md1 -= md2;

            Console.WriteLine(Environment.NewLine);
            DoSomething(ss, md1);

            Func<int, int, string> f1 = delegate(int a, int b)
            {
                return (a + b).ToString();
            };

            Action a1 = delegate() { Console.WriteLine("ACTION!"); };

            Console.ReadLine();
        }

        static void DoSomething(string[] items, MyDelegate m)
        {
            foreach (string s in items)
            {
                if (m != null)
                {
                    m(s);
                }
            }
        }
    }

    internal class StringMethods
    {
        public void PrintFirst(string s)
        {
            Console.WriteLine(s[0]);
        }

        public void PrintLast(string s)
        {
            Console.WriteLine(s.Last());
        }

        public void PrintObject(object o)
        {
            Console.WriteLine(o.ToString());
        }
    }
}