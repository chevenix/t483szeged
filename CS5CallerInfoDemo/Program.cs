﻿using System;
using System.Runtime.CompilerServices;

namespace CS5CallerInfoDemo
{
    class Program
    {
        static void Main()
        {
            Log();
            Console.ReadLine();
        }

        private static void Log([CallerFilePath]string filename = "", 
            [CallerLineNumber]int lineNumber = 0, 
            [CallerMemberName]string memberName = "")
        {
            Console.WriteLine("Filename: {0}, line: {1}, member: {2}", 
                filename, lineNumber, memberName);
        }
    }
}