﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace APMDemo
{
    class Program
    {
        static void Main()
        {
            string s = "alma";
            Func<string, int> f = GetLength;
            /*IAsyncResult iar =*/ f.BeginInvoke(s, GetLengthCompleted, f);
            //while (iar.IsCompleted == false)
            //{
            //    Console.Write(".");
            //    Thread.Sleep(200);
            //}
            //int i = f.EndInvoke(iar);
            //Console.WriteLine(i);

            Console.ReadLine();
        }

        private static void GetLengthCompleted(IAsyncResult ar)
        {
            Func<string, int> f = ar.AsyncState as Func<string, int>;
            int i = f.EndInvoke(ar);
            Console.WriteLine(i);
        }

        private static int GetLength(string s)
        {
            Thread.Sleep(3000);
            return s.Length;
        }
    }
}