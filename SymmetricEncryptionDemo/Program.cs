﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;

namespace SymmetricEncryptionDemo
{
    class Program
    {
        private const string ADATFILE = "adatok.txt";
        private const string TITKOSITOTTFILE = "titkos.txt";
        private const string VISSZAFEJTETTFILE = "visszafejtett.txt";

        static void Main()
        {
            SymmetricAlgorithm algo = new RijndaelManaged();
            //Rfc2898DeriveBytes keygen = new Rfc2898DeriveBytes("password", 
            //    Encoding.Unicode.GetBytes("my salt"));
            //algo.Key = keygen.GetBytes(algo.KeySize / 8);
            //algo.IV = keygen.GetBytes(algo.BlockSize / 8);
            algo.GenerateIV();
            algo.GenerateKey();

            //titkosítunk
            using (var adatfile = File.Open(ADATFILE, FileMode.Open))
            {
                FileStream titkosFile = File.Create(TITKOSITOTTFILE);
                ICryptoTransform encryptor = algo.CreateEncryptor();

                byte[] adatok = new byte[adatfile.Length];
                adatfile.Read(adatok, 0, adatok.Length);

                CryptoStream encryptStream = new CryptoStream(titkosFile, 
                    encryptor, CryptoStreamMode.Write);

                encryptStream.Write(adatok, 0, adatok.Length);
                encryptStream.Close();
                titkosFile.Close();
            }

            //visszafejtés
            using (var adatfile = File.Open(TITKOSITOTTFILE, FileMode.Open))
            {
                FileStream visszafejtettFile = File.Create(VISSZAFEJTETTFILE);
                ICryptoTransform decryptor = algo.CreateDecryptor();

                byte[] adatok = new byte[adatfile.Length];
                adatfile.Read(adatok, 0, adatok.Length);

                CryptoStream decryptStream = new CryptoStream(visszafejtettFile,
                    decryptor, CryptoStreamMode.Write);

                decryptStream.Write(adatok, 0, adatok.Length);
                decryptStream.Close();
                visszafejtettFile.Close();
            }
        }
    }
}