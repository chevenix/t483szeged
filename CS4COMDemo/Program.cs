﻿using System;
using System.IO;
using Microsoft.Office.Interop.Word;

namespace CS4COMDemo
{
    class Program
    {
        static void Main()
        {
            var word = new Application() { Visible = true };
            var doc = word.Documents.Add();
            doc.SaveAs2(FileName:
                Path.Combine(Environment.CurrentDirectory, @"\test.docx"));
            word.Quit();
            Console.ReadLine();
        }
    }
}