﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ParallelProgrammingDemo
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("MAIN: " + Thread.CurrentThread.ManagedThreadId);
            Thread t1 = new Thread(Doer.DoSomething);
            Thread t2 = new Thread(Doer.DoSomething);
            lock (typeof(Console))
            {
                t1.Start(ConsoleColor.Cyan);
                t2.Start(ConsoleColor.Yellow);

                t1.Join();
                t2.Join();
            }
            Console.WriteLine("Mindnek vége!");
            Console.ReadLine();
        }


    }

    public class Doer
    {
        private static readonly object _locker = new object();
        public static void DoSomething(object o)
        {
            ConsoleColor cc = (ConsoleColor)o;
            for (int i = 0; i < 10; i++)
            {
                lock (_locker)
                {
                    Console.ForegroundColor = cc;
                    Console.WriteLine(Thread.CurrentThread.ManagedThreadId);
                }
                Thread.Sleep(100);
            }
        }
    }
}