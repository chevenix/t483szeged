﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterPassingDemo
{
    class Program
    {
        static void Main()
        {
            string s = "alma";
            s += "béka";

            StringBuilder builder = new StringBuilder("alma", 300);
            Console.WriteLine(builder.ToString());
            AppendBéka(ref builder);
            Console.WriteLine(builder.ToString());
            Console.ReadLine();
        }

        private static void AppendBéka(ref StringBuilder sb)
        {
            sb = new StringBuilder();
            sb.Append("béka");
        }

        //private static void AppendBéka(StringBuilder sb)
        //{
        //    sb = new StringBuilder();
        //    sb.Append("béka");
        //}
    }
}