﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncAwaitDemo
{
    class Program
    {
        static void Main()
        {
            DoWork().Wait();

            //Task<string> t1 = Task.Factory.StartNew<string>(() => GetString())
            //    .ContinueWith(t => ReverseString(t.Result))
            //    .ContinueWith(t => UpperString(t.Result));

            Console.WriteLine("ofgabgfpifb");
            Console.ReadLine();
        }

        private static async Task DoWork()
        {
            Console.WriteLine("Elindult");
            string s1 = await GetStringAsync();
            Console.WriteLine("S1: " + s1);
            string s2 = await ReverseStringAsync(s1);
            Console.WriteLine("S2: " + s2);
            string s3 = await UpperStringAsync(s2);
            Console.WriteLine(s3);
        }

        private static Task<string> GetStringAsync()
        {
            return Task<string>.Factory.StartNew(() =>
            {
                Task.Delay(2000).Wait();
                return "alma";
            });
        }

        private static Task<string> ReverseStringAsync(string s)
        {
            return Task<string>.Factory.StartNew(() =>
            {
                Task.Delay(2000).Wait();
                return new string(s.Reverse().ToArray());
            });
        }

        private static Task<string> UpperStringAsync(string s)
        {
            return Task<string>.Factory.StartNew(() =>
            {
                Task.Delay(2000).Wait();
                return s.ToUpper();
            });
        }
    }
}