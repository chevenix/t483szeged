﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// ReSharper disable ReturnValueOfPureMethodIsNotUsed

namespace LinqToObjectsDemo
{
    class Program
    {
        static void Main()
        {
            List<int> list1 = Enumerable.Range(0, 10).ToList();
            List<int> list2 = Enumerable.Range(7, 5).ToList();

            var q1 = list1.Where(i => i % 2 == 0)
                .Select(i => new { Number = i, Squared = i * i });
            var q1b = (from i in list1
                       where i % 2 == 0
                       select new { Number = i, Squared = i * i }).ToList();
            //q1.Dump();

            list1.Add(12);

            Console.WriteLine();
            //q1.Dump();

            List<List<int>> listoflists = new List<List<int>>();
            listoflists.Add(new List<int> { 1, 2, 3 });
            listoflists.Add(new List<int> { 4, 5, 6 });
            var q2 = listoflists.SelectMany(l => l);
            //q2.Dump();
             
            var q3 = list1.Select(i => new { Number = i, Square = i * i })
                .OrderByDescending(x => x.Square.ToString().Last());

            var q3c = list1.Select(i => new { Number = i, Square = i * i })
    .OrderByDescending(x => x.Square.ToString().Last())
    .ThenBy(x => x.Square.ToString()[0]);

            var q3b = from x in
                (from i in list1
                 select new { Number = i, Square = i * i })
                      orderby x.Square.ToString().Last() descending
                      select x;

            //q3.Dump();
            Console.WriteLine();
            //q3c.Dump();

            var q4 = from i in list1
                     group i by i%2 == 0
                     into g
                     orderby g.Count() descending 
                     select new { Key = g.Key, Count = g.Count() };
            //q4.Dump();
            //foreach (var group in q4)
            //{
            //    Console.WriteLine(group.Key);
            //    foreach (int i in group)
            //    {
            //        Console.WriteLine("\t" + i);
            //    }
            //}

            var q5 = from i in list1
                join j in list2
                    on i equals j
                select i * j;
            //q5.Dump();

            var q6 = from i in listoflists
                let sum = i.Sum()
                orderby sum descending 
                select new { sum, i.Count, First = i.First() };
            //q6.Dump();

            //Partition(list1);
            //Quantifiers(list1);
            //Aggregates(list1);

            var union = list1.Union(list2); //Intersect, Except, Distinct

            var egyElem = list1.First(); //Last, ElementAt(int), Single, ...OrDefault

            list1.ToDictionary(i => i);

            object[] ok = new object[] {new object(), "alma", 1, 1.0};
            var doubles = ok.OfType<double>();

            var doubles2 = list1.Cast<double>();

            Console.ReadLine();
        }

        private static void Aggregates(List<int> list1)
        {
            int max = list1.Max();
            int min = list1.Min();
            double avg = list1.Average();
            //list1.Count()
        }

        private static void Quantifiers(List<int> list1)
        {
            bool any = list1.Any(i => i > 10);
            Console.WriteLine("Van 10-nél nagyobb: " + any);
            bool all = list1.All(i => i < 10);
            Console.WriteLine("Mindegyik kisebb, mint 10: " + all);
        }

        private static void Partition(List<int> list1)
        {
            for (int i = 0; i < list1.Count; i += 3)
            {
                var items = list1.Skip(i).Take(3);
                foreach (var item in items)
                {
                    Console.WriteLine(item);
                }
                Console.ReadLine();
            }
        }
    }

    static class EnumerableExtender
    {
        public static void Dump<T>(this IEnumerable<T> list)
        {
            foreach (T item in list) Console.WriteLine(item);
        }
    }
}