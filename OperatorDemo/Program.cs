﻿using System;

namespace OperatorDemo
{
    class Program
    {
        static void Main()
        {
            Pill p = new Pill("vörös");
            Wine w1 = p;

            Wine w2 = new Wine("fehér", 12.9);

            Wine w3 = w1 + w2;
            Console.WriteLine(w3.Percent);

            Console.ReadLine();
        }
    }
}