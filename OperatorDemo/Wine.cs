﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorDemo
{
    class Wine
    {
        private string _type;
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        private double _percent;

        public double Percent
        {
            get { return _percent; }
            set { _percent = value; }
        }

        public Wine(string type, double percent)
        {
            _type = type;
            _percent = percent;
        }

        public static Wine operator +(Wine a, Wine b)
        {
            return new Wine(GetWineType(a, b), (a.Percent + b.Percent) / 2);
        }

        private static string GetWineType(Wine a, Wine b)
        {
            if (a.Type == b.Type) return a.Type;
            else return "Rosé";
        }
    }
}
