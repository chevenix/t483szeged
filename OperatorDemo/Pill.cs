﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OperatorDemo
{
    class Pill
    {
        private string _wineType;
        public string WineType
        {
            get { return _wineType; }
            set { _wineType = value; }
        }

        public Pill(string wineType)
        {
            _wineType = wineType;
        }

        public static implicit operator Wine(Pill p)
        {
            return new Wine(p.WineType, 12.5);
        }
    }
}
