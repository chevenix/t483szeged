﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace XMLSerializationDemo
{
    class Program
    {
        static void Main()
        {
            XmlSerializer bf = new XmlSerializer(typeof(Person));
            if (File.Exists("stan.xml"))
            {
                using (var fs = File.Open("stan.xml", FileMode.Open))
                {
                    Person p = bf.Deserialize(fs) as Person;
                    Console.WriteLine("Név: {0}, kor: {1}, született: {2}",
                        p.Name, p.Age, p.BirthDate);
                }
            }
            else
            {
                Person p = new Person
                {
                    Name = "Stan",
                    Age = 8,
                    BirthDate = new DateTime(2009, 4, 6)
                };

                using (var fs = File.Create("stan.xml"))
                {
                    bf.Serialize(fs, p);
                    Console.WriteLine("sorosítva");
                }
            }
            Console.ReadLine();
        }
    }
}