﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XMLSerializationDemo
{
    public class Person
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private int _age;
        //[XmlIgnore]
        public int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        private DateTime _birthDate;
        public DateTime BirthDate
        {
            get { return _birthDate; }
            set { _birthDate = value; }
        }


        public Person(string name, int age, DateTime birthDate)
        {
            _name = name;
            _age = age;
            _birthDate = birthDate;
        }

        //ez az xml sorosításhoz mindenképpen kell
        public Person()
        {
            
        }
    }
}
