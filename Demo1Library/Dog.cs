﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    namespace Mammals
    {
        public class Dog
        {
            private string _firstName;
            public string FirstName
            {
                get { return _firstName; }
                set { _firstName = value; }
            }


            private string _lastName;
            public string LastName
            {
                get { return _lastName; }
                set { _lastName = value; }
            }
            
            public string FullName
            {
                get { return _firstName + " " + _lastName; }
            }

        }
    }
}

namespace Szomszédok
{
    public class Dog
    {
        public void M()
        {
            Animals.Mammals.Dog d = new Animals.Mammals.Dog();
        }
    }

}