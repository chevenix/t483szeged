﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlcoholDemo
{
    class Program
    {
        static void Main()
        {
            //Alcohol a1 = new Alcohol(11.5);

            //Alcohol a2 = new Alcohol(1);
            //a2.Percent = 4.5;
            //a2.Percent = 33;

            //Console.WriteLine("A1: " + a1.Percent);
            //Console.WriteLine("A2: " + a2.Percent);

            //Alcohol a3 = new Alcohol(1);
            //a2.Percent = Alcohol.MAXPERCENT;

            //Alcohol stronger = Alcohol.GetStronger(
            //    a1, a2, a1, a2, a3, a1, a1, a1, a1, a1);

            //Beer b = new Beer();

            //Casting();

            //EnumTest();
            
            //VirtualMethods();

            Tree t = new Tree();
            
            IBurnable ib = new Wine();
            ib.Burn();
            ib = t;
            ib.Burn();

            IBurnable2 ib2 = t;
            ib2.Burn();

            Console.ReadLine();
        }

        private static void BurnEverything(IBurnable[] items)
        {
            foreach (IBurnable item in items)
            {
                item.Burn();
            }
        }

        private static void VirtualMethods()
        {
            Alcohol a = new Wine();
            a.Drink();
        }

        private static void EnumTest()
        {
            Beer b = new Beer();
            b.Colour = BeerColour.Kék;
            Console.WriteLine(b.Colour);
        }

        private static void Casting()
        {
            Alcohol a = GetSomeBooze();

            //if (a.GetType() == typeof(Beer))

            //if (a is Beer)
            //{
            //    Beer b = (Beer)a;
            //    b.Colour = "alma";
            //}

            Beer b = a as Beer;
            if (b != null)
            {
                b.Colour =BeerColour.Kék;
            }

        }

        private static Alcohol GetSomeBooze()
        {
            if (new Random().Next(0, 2) == 0)
            {
                return new Beer();
            }
            else
            {
                return new Wine();
            }
        }
    }
}