﻿using System;

namespace AlcoholDemo
{
    class Wine : Alcohol
    {
        public Wine() : base(12.5)
        {}

        public sealed override void Drink()
        {
            Console.WriteLine("A bort borospohárból isszák.");
        }
    }
    class RedWine : Wine
    {
        //public override void Drink()
        //{
        //    Console.WriteLine("A vörösbort vörösborospohárból isszák.");
        //}
    }
}