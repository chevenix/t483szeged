﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlcoholDemo
{
    interface IBurnable
    {
        void Burn();
        //int I { get; set; }
        //event EventHandler MyEvent;
    }

    interface IVeryBurnable : IBurnable
    {
        void BurnVery();
    }

    interface IBurnable2
    {
        void Burn();
    }
}