﻿using System;

namespace AlcoholDemo
{
    class Tree : IBurnable, IBurnable2
    {
        void IBurnable.Burn()
        {
            Console.WriteLine("Felgyújtották a fát.");
        }
        void IBurnable2.Burn()
        {
            Console.WriteLine("Felgyújtották a fát KETTŐ.");
        }

        //public void Burn()
        //{
        //    Console.WriteLine("Harmadik...");
        //}
    }
}