﻿using System;

namespace AlcoholDemo
{
    class Beer : Alcohol
    {
        private BeerColour _colour;
        public BeerColour Colour
        {
            get { return _colour; }
            set { _colour = value; }
        }

        public Beer() : this(11.3)
        {
        }

        public Beer(double d) : base(d)
        {
            _colour = DetermineColour();
        }

        private BeerColour DetermineColour()
        {
            if (this.Percent < 5) return BeerColour.Világos;
            else return BeerColour.Barna;
        }

        public override void Drink()
        {
            Console.WriteLine("A sört korsóból isszák.");
        }


    }
}