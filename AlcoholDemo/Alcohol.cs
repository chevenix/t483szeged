﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlcoholDemo
{
    abstract class Alcohol : IBurnable
    {
        public static readonly int MAXPERCENT = 96;

        
        private double _percent =8;
        public double Percent
        {
            get { return _percent; }
            set
            {
                if (value > 0 && value <= MAXPERCENT)
                {
                    _percent = value;
                }
            }
        }

        public Alcohol(double p)
        {
            Percent = p;
        }

        static Alcohol()
        {
            if (DateTime.Now.DayOfWeek != (DayOfWeek.Saturday | DayOfWeek.Sunday))
            {
                MAXPERCENT = 96;
            }
            else
            {
                MAXPERCENT = 15;
            }
        }

        public static Alcohol GetStronger(Alcohol a, Alcohol b)
        {
            if (a._percent > b._percent) return a;
            else return b;
        }

        public static Alcohol GetStronger(Alcohol a, Alcohol b, Alcohol c)
        {
            //todo
            return null;
        }

        public static Alcohol GetStronger(Alcohol a, Alcohol b, 
            params Alcohol[] c)
        {
            Alcohol max = GetStronger(a, b);
            foreach (Alcohol alcohol in c)
            {
                if (alcohol._percent > max._percent) max = alcohol;
            }
            return max;
        }

        public abstract void Drink();

        public void Burn()
        {
            Console.WriteLine("Felgyújtották az alkoholt.");
        }
    }

}