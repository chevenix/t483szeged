﻿using System;

namespace ExceptionDemo
{
    class AgeException : Exception
    {
        public AgeException()
        {
            
        }

        public AgeException(Exception ex): base("", ex)
        {
        }
    }
}