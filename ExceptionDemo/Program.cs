﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionDemo
{
    class Program
    {
        static void Main()
        {
            try
            {
                int age = GetAge();
                if (age >= 18)
                {
                    Console.WriteLine("Te már nem vagy gyerek.");
                }
                else
                {
                    Console.WriteLine("Te még gyerek vagy.");
                }
            }
            catch (AgeException)
            {
                Console.WriteLine("nem megfelelő kor");
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Bekéri és ellenőrzi az életkort.
        /// </summary>
        /// <exception cref="ExceptionDemo.AgeException">AgeException</exception>
        /// <returns></returns>
        private static int GetAge()
        {
            Console.Write("Add meg a korod: ");
            try
            {
                int age = int.Parse(Console.ReadLine());
                if (age < 0 || age > 130)
                {
                    throw new AgeException();
                }
                return age;
            }
            catch (FormatException fex)
            {
                AgeException aex = new AgeException(fex);
                Console.WriteLine("FormatException");
                throw aex;
            }
            catch (OverflowException oex)
            {
                AgeException aex = new AgeException(oex);
                throw aex;
            }
            finally
            {
                Console.WriteLine("Vége");
            }
        }
    }
}