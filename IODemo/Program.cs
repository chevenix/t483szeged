﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace IODemo
{
    class Program
    {
        static void Main()
        {
            //FileInfoTest();
            //DirectoryInfoTest();
            //DriveInfoTest();
            //PathDemo();
            //FileIOTest();
            ReaderWriterDemo();

            Console.ReadLine();
        }

        private static void ReaderWriterDemo()
        {
            if (File.Exists("test.txt"))
            {
                using (var sr = new StreamReader("test.txt"))
                {
                    Console.WriteLine(sr.ReadToEnd());
                }
                using (var sw = new StreamWriter("test.txt", append: true))
                {
                    sw.WriteLine(DateTime.Now.ToLongTimeString());
                }
            }
            else
            {
                File.Create("test.txt");
                Console.WriteLine("Létrehozva.");
            }
        }

        private static void FileIOTest()
        {
            if (File.Exists("test.txt"))
            {
                using (var fs = File.Open("test.txt", FileMode.Open))
                {
                    byte[] bs = new byte[fs.Length];
                    fs.Read(bs, 0, bs.Length);
                    string s = Encoding.ASCII.GetString(bs);
                    Console.WriteLine(s);
                }

                string[] ss = File.ReadAllLines("test.txt");
                foreach (var s in ss) Console.WriteLine(s);
            }
            else
            {
                var fs = File.Create("test.txt");
                fs.WriteByte(65);
                fs.WriteByte(66);
                fs.WriteByte(67);
                fs.WriteByte(68);
                fs.WriteByte(69);
                //fs.Flush();
                //fs.Close();
                fs.Dispose();
            }
        }

        private static void PathDemo()
        {
            string path1 = @"C:\test\test.txt";
            string filename = Path.GetFileNameWithoutExtension(path1);
            Console.WriteLine(filename);
        }

        private static void DriveInfoTest()
        {
            foreach (var di in DriveInfo.GetDrives())
            {
                Console.WriteLine(di.Name);
                Console.WriteLine(di.DriveType);
                if (di.IsReady)
                {
                    Console.WriteLine(di.VolumeLabel);
                    Console.WriteLine(di.AvailableFreeSpace);
                }
            }
        }

        private static void DirectoryInfoTest()
        {
            DirectoryInfo di = new DirectoryInfo(@"C:\testdirectory");
            if (di.Exists)
            {
                Console.WriteLine("Van ilyen.");
                di.Delete();
            }
            else
            {
                Console.WriteLine("nincs ilyen.");
                di.Create();
            }
        }

        private static void FileInfoTest()
        {
            FileInfo fi = new FileInfo("test.txt");
            if (fi.Exists)
            {
                Console.WriteLine("Létezik.");
                fi.Delete();
            }
            else
            {
                Console.WriteLine("Nem van.");
                fi.Create();
            }
        }
    }
}