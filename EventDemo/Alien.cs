﻿using System;

namespace EventDemo
{
    class Alien //: IObserver
    {
        public void Probe(object sender, EventArgs e)
        {
            Human h = sender as Human;
            if (h != null)
            {
                Console.WriteLine("{0} pulzusa változott: {1}.", h.Name, h.Pulse);
            }
        }
    }
}