﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace EventDemo
{
    class Human
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private int _pulse;
        public int Pulse
        {
            get { return _pulse; }
            set
            {
                int prev = _pulse;
                _pulse = value;
                if (PulseChanged != null)
                {
                    PulseChanged(this, new PulseChangedEventArgs(prev));
                }
            }
        }

        //todo
        public event EventHandler PulseChanged;
    }

    //delegate void PulseChangedDelegate(Human h);

    class PulseChangedEventArgs : EventArgs
    {
        private int _prev;
        public int Previous
        {
            get { return _prev; }
        }

        public PulseChangedEventArgs(int prev)
        {
            _prev = prev;
        }
    }
}