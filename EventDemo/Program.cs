﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventDemo
{
    class Program
    {
        static void Main()
        {
            Human cartman = new Human();
            cartman.Name = "Cartman";

            Alien a1 = new Alien();
            Alien a2 = new Alien();

            cartman.PulseChanged += a1.Probe;
            cartman.PulseChanged += a2.Probe;

            cartman.Pulse = 120;

            //cartman.PulseChanged(new Human());

            Console.ReadLine();
        }
    }
}