﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsDemo
{
    partial class MyList : IEnumerable<int>
    {
        private int[] _items = new int[0];

        public void Add(int item)
        {
            Array.Resize(ref _items, _items.Length + 1);
            _items[_items.Length - 1] = item;
            this.M();
        }

        public int GetItem(int index)
        {
            return _items[index];
        }

        public IEnumerator<int> GetEnumerator()
        {
            //return new MyListIterator(this);
            yield return _items[4];
            yield return new Random().Next(0,100);

            for (int i = 0; i < _items.Length; i+=2)
            {
                yield return _items[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        //public class MyListIterator : IEnumerator<int>
        //{
        //    private MyList _list;
        //    private int _index = -1;

        //    public MyListIterator(MyList ml)
        //    {
        //        _list = ml;
        //    }

        //    public void Dispose()
        //    {
        //    }

        //    public bool MoveNext()
        //    {
        //        return ++_index < _list._items.Length;
        //    }

        //    public void Reset()
        //    {
        //    }

        //    public int Current
        //    {
        //        get { return _list._items[_index]; }
        //    }

        //    object IEnumerator.Current
        //    {
        //        get { return Current; }
        //    }
        //}
    }

    
}