﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericsDemo
{
    class Pair<T>
        where T : IComparable<T>
    {

        public static int Shared;

        private T _x;
        public T X
        {
            get { return _x; }
            set { _x = value; }
        }

        private T _y;
        public T Y
        {
            get { return _y; }
            set { _y = value; }
        }

        public Pair(T x, T y)
        {
            //if (x == null)
            //{
            //    _x = new T();
            //}
            _x = x;
            _y = y;
        }

        //public T GetSmaller(Comparison<T> comp)
        //{
        //    if (comp(X, Y) > 0)
        //    {
        //        return Y;
        //    }
        //    else
        //    {
        //        return X;
        //    }
        //}

        public T GetSmaller()
        {
            if (X.CompareTo(Y) > 0)
            {
                return Y;
            }
            return X;
        }

        public TRet Cast<TRet>(Func<T, TRet> converter)
        {
            return converter(Y);
        }
    }
}
