﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GenericsDemo
{
    class Program
    {
        static void Main()
        {
            Pair<int> ip = new Pair<int>(1, 2);
            Pair<string> sp = new Pair<string>("alma", "béka");
            sp.X = "7";

            //string s = ip.Cast(IntToString);
            string s = ip.Cast(new Func<int, string>(delegate (int x)
            {
                return x.ToString();
            }));
            //Console.WriteLine(s);

            int i = 3;
            int? j = null;
            j = i;
            i = (int)j;


            Iterator();


            Console.ReadLine();
        }

        private static void Iterator()
        {
            MyList ml = new MyList();
            for (int i = 0; i < 10; i++) ml.Add(i);

            foreach (int i in ml)
            {
                Console.WriteLine(i);
            }
        }

        private static string IntToString(int arg)
        {
            return arg.ToString();
        }
    }
}