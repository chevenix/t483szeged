﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsDemo
{
    class Apple
    {
        private int _size;
        public int Size
        {
            get { return _size; }
            set { _size = value; }
        }

    }

    class GreenApple : Apple
    {
        private double _greennessFactor;
        public double GreennessFactor
        {
            get { return _greennessFactor; }
            set { _greennessFactor = value; }
        }

    }
}
