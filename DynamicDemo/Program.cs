﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;

namespace DynamicDemo
{
    class Program
    {
        static void Main()
        {
            //dynamic s = "alma";
            //s = 8;
            //s.Alma();
            //Console.WriteLine(s);

            List<dynamic> nevesek = new List<dynamic>()
            {
                new Human { Name = "Jean-Luc Picard"},
                new Starship { Name = "Enterprise"}
            };
            nevesek.Sort(new DynamicNameComparer());

            foreach (var o in nevesek)
            {
                Console.WriteLine(o.Name);
            }

            dynamic mdc = new MyDynamicClass();
            mdc.aejrgivhdiuhbééogjféhjyéofgéodf();

            //string s = mdc(mdc)[mdc].Alma.Alma() + mdc;

            Console.ReadLine();
        }
    }

    class MyDynamicClass : DynamicObject
    {
        public void Alma()
        {
            Console.WriteLine("Alma");
        }

        public override bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
        {
            Console.WriteLine(binder.Name);
            result = null;
            return true;
        }
    }

    class DynamicNameComparer : IComparer<object>
    {
        public int Compare(dynamic x, dynamic y)
        {
            return x.Name.CompareTo(y.Name);
        }
    }

    class Human
    {
        public string Name { get; set; } 
    }

    class Starship
    {
        public string Name { get; set; }
    }
}